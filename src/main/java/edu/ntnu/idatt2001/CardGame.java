package edu.ntnu.idatt2001;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;


/**
 * A class representing a card game.
 */
public class CardGame extends Application {

  private DeckOfCards deck;
  private ArrayList<PlayingCard> hand;
  private Text sumText;
  private Text flushText;
  private Text containsS12Text;
  private Text allHeartsText;
  private Text handText;
  private Text rankingText;


  /**
   * Main method for the card game.
   *
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    launch(args);

  }

  @Override
  public void start(Stage primaryStage) {

    deck = new DeckOfCards();
    deck.shuffleDeck();
    hand = deck.dealHand(5);

    sumText = new Text("Sum: " + PlayerHand.getSum(hand));

    flushText = new Text("Flush: " + PlayerHand.isFlush(hand));

    containsS12Text = new Text("Contains S12: " + PlayerHand.hasQueenOfSpades(hand));

    allHeartsText = new Text("All Hearts: " + PlayerHand.hearts(hand));

    handText = new Text("Hand: " + PlayerHand.getHandAsString(hand));

    rankingText = new Text("Hand Ranking: " + PlayerHand.handRanking(hand));

    Button dealButton = new Button("Deal New Hand");
    dealButton.setOnAction(event -> dealNewHand());

    // Create the layout
    VBox layout = new VBox(10);
    layout.setPadding(new Insets(10));
    layout.getChildren().addAll(sumText, flushText, containsS12Text,
            allHeartsText, handText, rankingText, dealButton);

    // Show the window
    Scene scene = new Scene(layout);
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  private void dealNewHand() {
    deck.shuffleDeck();
    ArrayList<PlayingCard> hand = deck.dealHand(5);
    hand = new ArrayList<>(hand);

    sumText.setText("Sum: " + PlayerHand.getSum(hand));
    flushText.setText("Flush: " + PlayerHand.isFlush(hand));
    containsS12Text.setText("Contains S12: " + PlayerHand.hasQueenOfSpades(hand));
    allHeartsText.setText("All Hearts: " + PlayerHand.hearts(hand));
    handText.setText("Hand: " + PlayerHand.getHandAsString(hand));
    rankingText.setText("Hand Ranking: " + PlayerHand.handRanking(hand));

  }
}

