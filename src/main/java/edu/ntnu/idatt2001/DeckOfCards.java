package edu.ntnu.idatt2001;

import java.util.ArrayList;

/**
 * A class representing a deck of cards.
 */
public class DeckOfCards {
  private ArrayList<PlayingCard> deck;

  /**
   * Creates a new deck of cards.
   */
  public DeckOfCards() {
    deck = new ArrayList<>();
    char[] suit = {'S', 'H', 'D', 'C'};
    for (char s : suit) {
      int[] face = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
      for (int f : face) {
        deck.add(new PlayingCard(s, f));
      }
    }
  }

  public ArrayList<PlayingCard> getDeck() {
    return deck;
  }

  /**
   * Shuffles the deck of cards.
   */
  public void shuffleDeck() {
    ArrayList<PlayingCard> shuffledDeck = new ArrayList<>();
    while (deck.size() > 0) {
      int random = (int) (Math.random() * deck.size());
      shuffledDeck.add(deck.get(random));
      deck.remove(random);
    }
    deck = shuffledDeck;
  }

  /**
   * Deals a hand of n cards.
   *
   * @param n The number of cards to deal
   * @return An ArrayList of PlayingCard objects
   */
  public ArrayList<PlayingCard> dealHand(int n) {
    ArrayList<PlayingCard> copyDeck = new ArrayList<>(deck);
    ArrayList<PlayingCard> hand = new ArrayList<>();
    for (int i = 0; i < n; i++) {
      hand.add(copyDeck.get(i));
      copyDeck.remove(i);
    }
    return hand;
  }
}