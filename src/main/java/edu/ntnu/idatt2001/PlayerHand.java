package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * A class representing a player's hand.
 */
public class PlayerHand {
  /**
   * Sorts the hand by face value.
   *
   * @param hand The hand to sort
   */
  public static void sortHand(ArrayList<PlayingCard> hand) {
    hand.sort(Comparator.comparingInt(PlayingCard::face));
  }

  /**
   * Returns the sum of all cards in the hand.
   *
   * @param hand The hand to check
   * @return The sum of all cards in the hand
   */
  public static int getSum(ArrayList<PlayingCard> hand) {
    return hand.stream().mapToInt(PlayingCard::face).sum();
  }

  /**
   * Returns a list of all hearts in the hand.
   *
   * @param hand The hand to check
   * @return A list of all hearts in the hand
   */
  public static ArrayList<String> hearts(ArrayList<PlayingCard> hand) {
    ArrayList<String> hearts = new ArrayList<>();
    for (PlayingCard playingCard : hand) {
      if (playingCard.suit() == 'H') {
        hearts.add(playingCard.getAsString());
      }
    }
    return hearts;
  }

  public static String getHandAsString(ArrayList<PlayingCard> hand) {
    StringBuilder handAsString = new StringBuilder();
    for (PlayingCard playingCard : hand) {
      handAsString.append(playingCard.getAsString()).append(" ");
    }
    return handAsString.toString();
  }

  /**
   * Checks if S12 is in the hand.
   *
   * @param hand The hand to check
   * @return True if S12 is in the hand or false if it is not
   */
  public static boolean hasQueenOfSpades(ArrayList<PlayingCard> hand) {
    return hand.stream().anyMatch(playingCard ->
            playingCard.suit() == 'S' && playingCard.face() == 12);
  }

  /**
   * Checks if the hand is a flush.
   *
   * @param hand The hand to check
   * @return True if the hand is a flush, false otherwise
   */
  public static boolean isFlush(ArrayList<PlayingCard> hand) {
    return hand.stream().allMatch(playingCard -> playingCard.suit() == hand.get(0).suit());
  }

  /**
   * Checks if the hand is a straight.
   *
   * @param hand The hand to check
   * @return True if the hand is a straight, false otherwise
   */
  public static boolean isStraight(ArrayList<PlayingCard> hand) {
    boolean isStraight = true;
    sortHand(hand);
    ArrayList<Integer> faceValues = new ArrayList<>();
    for (PlayingCard playingCard : hand) {
      faceValues.add(playingCard.face());
    }
    for (int i = 1; i < faceValues.size(); i++) {
      if (faceValues.get(i) != faceValues.get(i - 1) + 1) {
        isStraight = false;
        break;
      }
    }
    return isStraight;
  }

  /**
   * Checks if the hand is a straight flush.
   *
   * @param hand The hand to check
   * @return True if the hand is a straight flush, false otherwise
   */
  public static boolean isStraightFlush(ArrayList<PlayingCard> hand) {
    return isStraight(hand) && isFlush(hand);
  }

  /**
   * Checks if the hand has four of a kind.
   *
   * @param hand The hand to check
   * @return True if the hand is a royal flush, false otherwise
   */
  public static boolean hasFourOfKind(ArrayList<PlayingCard> hand) {
    boolean hasFourOfKind = false;
    for (int i = 0; i < hand.size(); i++) {
      int count = 0;
      for (PlayingCard playingCard : hand) {
        if (hand.get(i).face() == playingCard.face()) {
          count++;
        }
        if (count == 4) {
          hasFourOfKind = true;
        }
      }
    }
    return hasFourOfKind;
  }

  /**
   * Checks if the hand has a three of a kind.
   *
   * @param hand The hand to check
   * @return True if the hand has a three of a kind, false otherwise
   */
  public static boolean hasThreeOfKind(ArrayList<PlayingCard> hand) {
    boolean threeOfKind = false;
    for (int i = 0; i < hand.size(); i++) {
      int count = 0;
      for (PlayingCard playingCard : hand) {
        if (hand.get(i).face() == playingCard.face()) {
          count++;
        }
        if (count == 3) {
          threeOfKind = true;
        }
      }
    }
    return threeOfKind;
  }

  /**
   * Checks if the hand has a pair.
   *
   * @param hand The hand to check
   * @return True if the hand has a pair, false otherwise
   */
  public static boolean hasPair(ArrayList<PlayingCard> hand) {
    boolean pair = false;
    for (int i = 0; i < hand.size(); i++) {
      int count = 0;
      for (PlayingCard playingCard : hand) {
        if (hand.get(i).face() == playingCard.face()) {
          count++;
        }
        if (count == 2) {
          pair = true;
        }
      }
    }
    return pair;
  }

  /**
   * Checks if the hand has two pairs.
   *
   * @param hand The hand to check
   * @return True if the hand has two pairs, false otherwise
   */
  public static boolean hasTwoPairs(ArrayList<PlayingCard> hand) {
    int pairCount = 0;
    for (int i = 0; i < hand.size(); i++) {
      for (int j = i + 1; j < hand.size(); j++) {
        if (hand.get(i).face() == hand.get(j).face()) {
          pairCount++;
        }
      }
    }
    return pairCount == 2;
  }

  /**
   * Checks if the hand has a full house.
   *
   * @param hand The hand to check
   * @return True if the hand has a full house, false otherwise
   */
  public static boolean hasFullHouse(ArrayList<PlayingCard> hand) {
    int[] faceCounts = new int[14];
    for (PlayingCard card : hand) {
      int faceValue = card.face();
      faceCounts[faceValue]++;
    }
    boolean hasThreeOfKind = false;
    boolean hasPair = false;
    for (int count : faceCounts) {
      if (count == 3) {
        hasThreeOfKind = true;
      } else if (count == 2) {
        hasPair = true;
      }
    }
    return hasThreeOfKind && hasPair;
  }

  /**
   * Checks if the hand has a royal flush were ace is one.
   *
   * @param hand The hand to check
   * @return True if the hand has a royal flush, false otherwise
   */
  public static boolean hasRoyalFlush(ArrayList<PlayingCard> hand) {
    sortHand(hand);
    return isFlush(hand) && hand.get(0).face() == 1 && hand.get(1).face() == 10
            && hand.get(2).face() == 11 && hand.get(3).face() == 12 && hand.get(4).face() == 13;
  }

  /**
   * Checks for highest card in a hand.
   *
   * @param hand The hand to check
   * @return The highest card in the hand
   */
  public static PlayingCard highestCard(ArrayList<PlayingCard> hand) {
    PlayingCard highestCard = hand.get(0);
    for (PlayingCard playingCard : hand) {
      if (playingCard.face() > highestCard.face()) {
        highestCard = playingCard;
      }
    }
    return highestCard;
  }

  /**
   * Returns the highest ranking hand has.
   *
   * @param hand The hand to check
   */
  public static String handRanking(ArrayList<PlayingCard> hand) {
    if (hand.size() != 5) {
      return "Hand must have 5 cards to be ranked";
    } else if (hasRoyalFlush(hand)) {
      return "Royal Flush";
    } else if (isStraightFlush(hand)) {
      return "Straight Flush";
    } else if (hasFourOfKind(hand)) {
      return "Four of a Kind";
    } else if (hasFullHouse(hand)) {
      return "Full House";
    } else if (isFlush(hand)) {
      return "Flush";
    } else if (isStraight(hand)) {
      return "Straight";
    } else if (hasThreeOfKind(hand)) {
      return "Three of a Kind";
    } else if (hasTwoPairs(hand)) {
      return "Two Pairs";
    } else if (hasPair(hand)) {
      return "Pair";
    } else {
      return "High Card: " + highestCard(hand).getAsString();
    }
  }
}

