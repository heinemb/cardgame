package edu.ntnu.idatt2001;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {

  private final PlayingCard playingCardTest = new PlayingCard('S', 12);
  @Test
  @DisplayName("Check if both suit and face is correct")
  void getAsString() {
    assertEquals("S12", playingCardTest.getAsString());
  }

  @Test
  @DisplayName("Check if the suit is correct")
  void getSuit() {
    assertEquals('S', playingCardTest.suit());
  }

  @Test
  @DisplayName("Check if the face is correct")
  void getFace() {
    assertEquals(12, playingCardTest.face());
  }
}