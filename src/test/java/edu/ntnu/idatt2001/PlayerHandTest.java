package edu.ntnu.idatt2001;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;


class PlayerHandTest {

  private final ArrayList<PlayingCard> hand = new ArrayList<>();
  private final DeckOfCards deck = new DeckOfCards();


  public void addCardsToHand (){
    hand.add(new PlayingCard('H', 5));
    hand.add(new PlayingCard('S', 2));
    hand.add(new PlayingCard('H', 3));
    hand.add(new PlayingCard('C', 4));
    hand.add(new PlayingCard('H', 1));
  }
  @Test
  @DisplayName("Check if the hand is not 5 cards")
  void notFiveCards() {
    assertNotEquals(5, deck.dealHand(4).size());
  }
  @Test
  @DisplayName("Check if the hand is 5 cards")
  void fiveCards() {
    assertEquals(5, deck.dealHand(5).size());
  }

  @Test
  @DisplayName("Check if the hand is sorted by face value")
  void sortHand() {
    addCardsToHand();
    PlayerHand.sortHand(hand);
    assertEquals(1, hand.get(0).face());
    assertEquals(2, hand.get(1).face());
    assertEquals(3, hand.get(2).face());
    assertEquals(4, hand.get(3).face());
    assertEquals(5, hand.get(4).face());
  }

  @Test
  @DisplayName("Check if summed value of cards is correct")
  void sumOfCards() {
    addCardsToHand();
    assertEquals(15, PlayerHand.getSum(hand));
  }

  @Test
  @DisplayName("Check if method returns all hearts in hand")
  void hearts() {
    addCardsToHand();
    assertEquals(3, PlayerHand.hearts(hand).size());
  }

  @Test
  @DisplayName("Check if hand contains a queen of spades")
  void hasQueenOfSpades() {
    addCardsToHand();
    assertFalse(PlayerHand.hasQueenOfSpades(hand));
    hand.add(new PlayingCard('S', 12));
    assertTrue(PlayerHand.hasQueenOfSpades(hand));
  }

  @Test
  void isFlush() {
    addCardsToHand();
    assertFalse(PlayerHand.isFlush(hand));
    hand.removeAll(hand);
    hand.add(new PlayingCard('H', 5));
    hand.add(new PlayingCard('H', 2));
    hand.add(new PlayingCard('H', 3));
    hand.add(new PlayingCard('H', 7));
    hand.add(new PlayingCard('H', 4));
    assertTrue(PlayerHand.isFlush(hand));

  }

  @Test
  void isStraight() {
    addCardsToHand();
    assertTrue(PlayerHand.isStraight(hand));
    hand.remove(1);
    hand.add(new PlayingCard('H', 7));
    assertFalse(PlayerHand.isStraight(hand));
  }

  @Test
  void isStraightFlush() {
    addCardsToHand();
    assertFalse(PlayerHand.isStraightFlush(hand));
    hand.removeAll(hand);
    hand.add(new PlayingCard('H', 5));
    hand.add(new PlayingCard('H', 2));
    hand.add(new PlayingCard('H', 3));
    hand.add(new PlayingCard('H', 4));
    hand.add(new PlayingCard('H', 1));
    assertTrue(PlayerHand.isStraightFlush(hand));
  }

  @Test
  void hasFourOfKind() {
    addCardsToHand();
    assertFalse(PlayerHand.hasFourOfKind(hand));
    hand.removeAll(hand);
    hand.add(new PlayingCard('H', 4));
    hand.add(new PlayingCard('S', 4));
    hand.add(new PlayingCard('C', 4));
    hand.add(new PlayingCard('D', 4));
    hand.add(new PlayingCard('S', 1));
    assertTrue(PlayerHand.hasFourOfKind(hand));
  }

  @Test
  void hasThreeOfAKind() {
    addCardsToHand();
   assertFalse(PlayerHand.hasThreeOfKind(hand));
      hand.removeAll(hand);
      hand.add(new PlayingCard('H', 4));
      hand.add(new PlayingCard('S', 4));
      hand.add(new PlayingCard('C', 4));
      hand.add(new PlayingCard('D', 1));
      hand.add(new PlayingCard('S', 1));
      assertTrue(PlayerHand.hasThreeOfKind(hand));
  }

  @Test
  void hasPair() {
    addCardsToHand();
    assertFalse(PlayerHand.hasPair(hand));
    hand.removeAll(hand);
    hand.add(new PlayingCard('H', 4));
    hand.add(new PlayingCard('S', 4));
    hand.add(new PlayingCard('C', 1));
    hand.add(new PlayingCard('D', 1));
    hand.add(new PlayingCard('S', 1));
    assertTrue(PlayerHand.hasPair(hand));
  }

  @Test
  void hasTwoPairs() {
    addCardsToHand();
    assertFalse(PlayerHand.hasTwoPairs(hand));
    hand.removeAll(hand);
    hand.add(new PlayingCard('H', 4));
    hand.add(new PlayingCard('S', 4));
    hand.add(new PlayingCard('C', 1));
    hand.add(new PlayingCard('D', 1));
    hand.add(new PlayingCard('S', 2));
    assertTrue(PlayerHand.hasTwoPairs(hand));
  }

  @Test
  void hasFullHouse() {
    addCardsToHand();
    assertFalse(PlayerHand.hasFullHouse(hand));
    hand.removeAll(hand);
    hand.add(new PlayingCard('H', 4));
    hand.add(new PlayingCard('S', 4));
    hand.add(new PlayingCard('C', 4));
    hand.add(new PlayingCard('D', 1));
    hand.add(new PlayingCard('S', 1));
    assertTrue(PlayerHand.hasFullHouse(hand));
  }

  @Test
  void hasRoyalFlushWithStreams() {
    addCardsToHand();
    assertFalse(PlayerHand.hasRoyalFlush(hand));
    hand.removeAll(hand);
    hand.add(new PlayingCard('H', 1));
    hand.add(new PlayingCard('H', 10));
    hand.add(new PlayingCard('H', 11));
    hand.add(new PlayingCard('H', 12));
    hand.add(new PlayingCard('H', 13));
    assertTrue(PlayerHand.hasRoyalFlush(hand));
  }

  @Test
  void highestCard() {
    addCardsToHand();
    assertEquals(5, PlayerHand.highestCard(hand).face());
    hand.removeAll(hand);
    hand.add(new PlayingCard('H', 1));
    hand.add(new PlayingCard('H', 10));
    hand.add(new PlayingCard('H', 11));
    hand.add(new PlayingCard('H', 12));
    hand.add(new PlayingCard('H', 13));
    assertEquals(13, PlayerHand.highestCard(hand).face());
  }
}