package edu.ntnu.idatt2001;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
  private final DeckOfCards deck = new DeckOfCards();

  @Test
  @DisplayName("Test if deck is returned")
  void getDeck() {
    assertEquals(52, deck.getDeck().size());
  }

  @Test
  @DisplayName("Test if deck is shuffled")
  void shuffleDeck() {
    ArrayList<PlayingCard> deck1 = deck.getDeck();
    deck.shuffleDeck();
    ArrayList<PlayingCard> deck2 = deck.getDeck();
    assertNotEquals(deck1, deck2);
  }

  @Test
  @DisplayName("Test if hand of n cards is dealt")
  void dealHand() {
    assertEquals(5, deck.dealHand(5).size());
  }
}